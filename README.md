# limiti_istat_2021

## vettori

1. Com01012021_WGS84.geojson
2. ProvCM01012021_WGS84.geojson
3. Reg01012021_WGS84.geojson
4. Reg01012021_WGS84_PA.geojson (con province autonome)

derivano dagli https://www.istat.it/it/archivio/222527, trasformati e semplificati (usando https://mapshaper.org/ visvalingam 1.5%) in formato `geojson` standard RFC7946.
